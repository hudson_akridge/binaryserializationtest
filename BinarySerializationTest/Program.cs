﻿using System;
using System.IO;
using MsgPack;
using ProtoBuf;
using ProtoBuf.Meta;
using SerializationContext = MsgPack.Serialization.SerializationContext;

namespace BinarySerializationTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var msgPackTestObject = new SubClass { FirstName = "Hudson", LastName = "Akridge" };
            var msgPackTestObjectTwo = new SubClassTwo { FirstName = "Hudson", Age = 33, ExtClass = new ExternalClass { SomethingComplex = new byte[60] } };


            ProtoBufTest(msgPackTestObject, msgPackTestObjectTwo);
            MessagePackTest(msgPackTestObject, msgPackTestObjectTwo);
        }

        private static void ProtoBufTest(SubClass msgPackTestObject, SubClassTwo msgPackTestObjectTwo)
        {
            const string fileNameOne = "MsgPackObjOne.tst";
            const string fileNameTwo = "MsgPackObjTwo.tst";

            var streamTwo = new FileStream(fileNameTwo, FileMode.Create);
            var streamOne = new FileStream(fileNameOne, FileMode.Create);

            RuntimeTypeModel.Default.Add(typeof(BaseClass), false).Add("FirstName")
                .AddSubType(2, typeof(SubClass))
                .AddSubType(3, typeof(SubClassTwo));
            RuntimeTypeModel.Default.Add(typeof(SubClass), false).Add("LastName");
            RuntimeTypeModel.Default.Add(typeof(SubClassTwo), true).Add("Age");
            Serializer.Serialize(streamOne, msgPackTestObject);
            Serializer.Serialize(streamTwo, msgPackTestObjectTwo);

            streamOne.Close();
            streamTwo.Close();

            var deserializerStreamTwo = new FileStream(fileNameTwo, FileMode.Open);
            var deserializerStreamOne = new FileStream(fileNameOne, FileMode.Open);

            var deserializedResultOne = Serializer.Deserialize<BaseClass>(deserializerStreamOne);
            var deserializedResultTwo = Serializer.Deserialize<BaseClass>(deserializerStreamTwo);

            deserializerStreamOne.Close();
            deserializerStreamTwo.Close();
        }

        private static void MessagePackTest(SubClass msgPackTestObject, SubClassTwo msgPackTestObjectTwo)
        {
            const string fileNameOne = "ProtoBufObjOne.tst";
            const string fileNameTwo = "ProtoBufObjTwo.tst";

            var streamTwo = new FileStream(fileNameTwo, FileMode.Create);
            var streamOne = new FileStream(fileNameOne, FileMode.Create);

            var serializerOne = new SerializationContext(PackerCompatibilityOptions.PackBinaryAsRaw).GetSerializer<SubClass>();
            serializerOne.Pack(streamOne, msgPackTestObject);
            streamOne.Close();


            var serializerTwo = new SerializationContext(PackerCompatibilityOptions.PackBinaryAsRaw).GetSerializer<SubClassTwo>();
            serializerTwo.Pack(streamTwo, msgPackTestObjectTwo);
            streamTwo.Close();

            var streamReaderOne = new FileStream(fileNameOne, FileMode.Open);
            var objDeserializerOne = new SerializationContext(PackerCompatibilityOptions.PackBinaryAsRaw);
            var unpackedObject = objDeserializerOne.GetSerializer<dynamic>()
                .Unpack(streamReaderOne);
            var unpackedObjectJson = unpackedObject.ToString();

            var streamReaderTwo = new FileStream(fileNameTwo, FileMode.Open);
            var objDeserializerTwo = SerializationContext.Default.GetSerializer<dynamic>();
            var unpackedObjectTwo = objDeserializerTwo.Unpack(streamReaderTwo);
            var unpackedObjectTwoJson = unpackedObjectTwo.ToString();

            streamReaderOne.Close();
            streamReaderTwo.Close();
        }
    }

    [Serializable]
    public abstract class BaseClass
    {
        public string FirstName { get; set; }
    }

    [Serializable]
    public class SubClass : BaseClass
    {
        public string LastName { get; set; }
    }

    [Serializable]
    public class SubClassTwo : BaseClass
    {
        public int Age { get; set; }
        public ExternalClass ExtClass { get; set; }
    }

    [Serializable]
    public class ExternalClass
    {
        public ExternalClass()
        {
            SomethingComplex = new byte[5];
        }
        public byte[] SomethingComplex { get; set; }
    }
}
